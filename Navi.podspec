Pod::Spec.new do |s|
  s.name         = "Navi"
  s.version      = "0.1.0"
  s.summary      = "Helper for navigating between view controllers"
  s.homepage     = "https://bitbucket.org/prolificinteractive/Navi"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Harlan Kellaway" => "harlan@prolificinteractive.com" }
  s.source       = { :git => 'git@bitbucket.org:prolificinteractive/Navi.git', :tag => s.version.to_s }
  
  s.platform     = :ios, "8.0"
  s.requires_arc = true

  s.source_files  = 'Pod/Classes/*.{h,swift}'

end
