//
//  UINavigationController.swift
//  Navi
//
// Copyright (c) 2015 Prolific Interactive
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

extension UINavigationController {
    
    /**
    Pushes a view controller of the provided type
    */
    public func pushViewControllerOfType<T: Navigatable>(type: T.Type) -> () {
        self.pushViewControllerOfType(type) { _ in }
    }
    
    /**
    Pushes a view controller of the provided type and call completion with pushed controller
    */
    public func pushViewControllerOfType<T: Navigatable>(type: T.Type, completion: UIViewController? -> ()) -> () {
        let controller = UINavigationController.getController(type)
        
        if let controller = controller {
            self.pushViewController(controller, animated: true)
            completion(controller)
        }
        
        completion(nil)
    }
    
    // MARK: - Private methods
    
    /**
    Returns an instance of the provided view controller type
    */
    private static func getController<T: Navigatable>(controller: T.Type) -> UIViewController? {
        if(controller is UIViewController.Type) {
            let storyboard = UIStoryboard(name: controller.storyboardName(), bundle: nil)
            let viewController = storyboard.instantiateViewControllerWithIdentifier(controller.controllerName()) as! T
            
            return (viewController as! UIViewController)
        }
        
        return nil
    }
}
