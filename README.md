# Navi

## Purpose

The purpose of this pod is to provide a more intuitive API for the pattern of pushing view controllers instantiated from Storyboards.

## Authors

### iOS

* Harlan Kellaway

## Example

One way to handle navigating between controllers besides segues is to instantiate the controller from its Storyboard and push that controller. Adopting this pattern has each controller that is to be navigated to implementing something like:

``` swift
import UIKit

class SecondViewController: UIViewController {

    static func instantiateFromStoryboard() -> SecondViewController {
        let storyboard = UIStoryboard(name: "SecondStoryboard", bundle: nil)
        let viewController = storyboard.instantiateViewControllerWithIdentifier("SecondViewController") as! SecondViewController
        
        return viewController
    }
}
```

The logic of initializing a Storyboard and instantiating a view controller often ends up repeated - whereas the only unique information is the Storyboard name (i.e. "SecondStoryboard") and the controller identifier (i.g. "SecondViewController").

Navi makes it so the view controller being navigated to simply has to provide that information:

``` swift
import Navi
import UIKit

class SecondViewController: UIViewController, Navigatable {

    static func storyboardName() -> String {
        return "SecondStoryboard"
    }
    
    static func controllerName() -> String {
        return "SecondViewController"
    }
    
}

```

Now, controllers that want to navigate to this conroller go from:

``` swift
let viewController = SecondViewController.instantiateFromStoryboard()
        
self.navigationController?.pushViewController(viewController, animated: true)
```

to

``` swift
self.navigationController?.pushViewControllerOfType(SecondViewController)
```

Nice!

## iOS Specificities

#### Dev Notes

##### Pods Used

#### Install/Build Notes
* Clone repo
* install pods (`pod install`)
